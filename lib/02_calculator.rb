def add(num1, num2)
  return num1 + num2
end

def subtract(num1, num2)
  return num1 - num2
end

def sum array
  if array.length == 0
    return 0
  end

  sum = 0
  array.each do |curr_parameter|
    sum += curr_parameter
  end
  sum
end
