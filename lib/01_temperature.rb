def ftoc parameter
   return (parameter - 32) * 5 / 9
end

def ctof parameter
  return (parameter * 9 / 5.0) + 32
end
