def echo parameter
  return parameter
end

def shout parameter
  return parameter.upcase
end

def repeat (parameter, num = 2)
  arr = []
  num.times {arr.push(parameter)}
  arr.join(' ')
end

def start_of_word(word, num)
  return word.slice(0,num)
end

def first_word(string)
  return string.split(' ')[0]
end

def titleize(string)
  words = string.split(' ')
  new_arr = []
  skip_words = "a or the over and".split(' ')
  words.each_with_index do |word, index|
    if index != 0 && skip_words.index(word) != nil
      new_arr.push(word)
    else
      new_arr.push(word.capitalize)
    end
  end
  new_arr.join(' ')
end
