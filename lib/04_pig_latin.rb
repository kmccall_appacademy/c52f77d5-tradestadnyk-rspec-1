def translate(string)
  words = string.split(' ')
  vowels = 'aeioAEIO'.split('')
  final_arr = []
  words.each do |word|
    if(vowels.index(word[0]) != nil)
      final_arr.push(word + "ay")
    else
      final_arr.push(new_cons_word(word))
    end
  end
  final_arr.join(' ')
end

def new_cons_word(word)
  vowels = 'aeioAEIO'.split('')
  word.each_char.with_index do |char, index|
    if vowels.index(char) != nil
      return word.slice(index, word.length) + word.slice(0, index) + "ay"
    end
  end
end
